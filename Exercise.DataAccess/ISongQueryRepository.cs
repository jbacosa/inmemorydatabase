﻿using Domain.Core.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Exercise.DataAccess
{
    public interface ISongQueryRepository
    {
        Task<List<SongDto>> GetSongs();
    }
}
