﻿using Domain.Core.Dto;
using Exercise.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Exercise.DataAccess
{
    public class SongQueryRepository : ISongQueryRepository
    {
        private readonly IInMemoryDbContext _inMemoryDbContext;
        public SongQueryRepository(IInMemoryDbContext inMemoryDbContext)
        {
            _inMemoryDbContext = inMemoryDbContext;
        }

        public async Task<List<SongDto>> GetSongs()
        {
            var result = await _inMemoryDbContext.GetSong();
            var response = new List<SongDto>()
            {
                new SongDto()
                {
                    Id = result.Id,
                    SongTitle = result.SongTitle
                }
            };
            return response;
        }
    }
}
