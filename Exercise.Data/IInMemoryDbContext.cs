﻿using Domain.Core.Dto;
using System.Threading.Tasks;

namespace Exercise.Data
{
    public interface IInMemoryDbContext
    {
        Task<int> Save(SongDto song);
        Task<SongDto> GetSong();
    }
}
