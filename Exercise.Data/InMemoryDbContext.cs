﻿using Domain.Core.Dto;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace Exercise.Data
{
    public class InMemoryDbContext : IInMemoryDbContext
    {
        DbContextOptions<ApplicationDbContext> _options;
        public InMemoryDbContext()
        {
             _options = new DbContextOptionsBuilder<ApplicationDbContext>()
                       .UseInMemoryDatabase(databaseName: "SongsDatabase")
                       .Options;
        }

        
        public async Task<int> Save(SongDto song)
        {
          

            using (var context = new ApplicationDbContext(_options))
            {
                context.Songs.Add(new SongEntity()
                {
                    Title = song.SongTitle
                });
                return await context.SaveChangesAsync();
            }


           
        }

        public async Task<SongDto> GetSong()
        {
            using (var context = new ApplicationDbContext(_options))
            {

                var song = await context.Songs.FirstOrDefaultAsync();
                return new SongDto() {
                     SongTitle = song.Title
                };
            }
        }

    }
}
