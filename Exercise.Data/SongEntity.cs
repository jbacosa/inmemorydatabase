﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Exercise.Data
{
    public class SongEntity
    {
        public int Id { get; set; }
        public string Title { get; set; }
    }
}
