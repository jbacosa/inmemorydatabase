﻿using Domain.Core.Dto;
using Exercise.DataAccess;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Application.SongCommand
{
    public interface ISongCommand
    {
        Task<int> CreateSong(SongDto song);
    }
}
