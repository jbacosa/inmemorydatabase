﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Domain.Core.Dto;
using Exercise.DataAccess;
using Exercise.Persistence;

namespace Domain.Application.SongCommand
{
    public class SongCommand : ISongCommand
    {
        private readonly ISongPersistenceRepository _songPersistenceRepository;
        public SongCommand(ISongPersistenceRepository songPersistenceRepository)
        {
            _songPersistenceRepository = songPersistenceRepository;
        }
        public async Task<int> CreateSong(SongDto song)
        {
            return await _songPersistenceRepository.SaveSong(song);
        }
    }
}
