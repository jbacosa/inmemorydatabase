﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Core.Dto
{
    public class SongDto
    {
        public int Id { get; set; }
        public string SongTitle { get; set; }
    }
}
