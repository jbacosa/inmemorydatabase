﻿using Domain.Core.Dto;
using Exercise.Data;
using System.Threading.Tasks;

namespace Exercise.Persistence
{
    public class SongPersistenceRepository : ISongPersistenceRepository
    {
        private readonly IInMemoryDbContext _inMemoryDbContext;
        public SongPersistenceRepository(IInMemoryDbContext inMemoryDbContext)
        {
            _inMemoryDbContext = inMemoryDbContext;
        }
        public async Task<int> SaveSong(SongDto song)
        {
           
            return await _inMemoryDbContext.Save(song);
        }
    }
}
