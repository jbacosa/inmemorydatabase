﻿using Domain.Core.Dto;
using System.Threading.Tasks;

namespace Exercise.Persistence
{
    public interface ISongPersistenceRepository
    {
        Task<int> SaveSong(SongDto song);
    }
}
