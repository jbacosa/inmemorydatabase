﻿using Domain.Core.Dto;
using Exercise.DataAccess;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Query
{
    public interface ISongQuery
    {
        Task<IReadOnlyList<SongDto>> GetSongs();
    }
}
