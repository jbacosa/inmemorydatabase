﻿using Domain.Core.Dto;
using Exercise.DataAccess;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Query
{
    public class SongQuery : ISongQuery
    {
        private readonly ISongQueryRepository _songQueryRepository;
        public SongQuery(ISongQueryRepository songQueryRepository)
        {
            _songQueryRepository = songQueryRepository;
        }

        public async Task<IReadOnlyList<SongDto>> GetSongs()
        {
            return await _songQueryRepository.GetSongs();
        }
    }
}
