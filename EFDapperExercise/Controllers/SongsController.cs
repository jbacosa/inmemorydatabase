﻿using Domain.Application.SongCommand;
using Domain.Core.Dto;
using Domain.Query;
using Exercise.DataAccess;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EFDapperExercise.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SongsController : ControllerBase
    {
        private readonly ISongQuery _songQuery;
        private readonly ISongCommand _songCommand;
        public SongsController(ISongQuery songQuery, ISongCommand songCommand)
        {
            _songQuery = songQuery;
            _songCommand = songCommand;
        }

        [HttpGet]        
        public async Task<ActionResult<IReadOnlyList<SongDto>>> GetSongs() => Ok(await _songQuery.GetSongs());

        [HttpPost]
        public async Task<ActionResult<int>> CreateSong(SongDto song) {

            var result = await _songCommand.CreateSong(song);
            return CreatedAtAction("GetSongs", "Songs", new { id = result }, result);
        }
    }
}